import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
public class Sorting {
    public static void main (String[] args){
        boolean repeat = false;
        BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("-- Program Mengurutkan Barang --");
        do{
            try{
                try{
                    mainMenu();
                    System.out.print("Pilih Menu ==>");
                    String choiceString = brInput.readLine();
                    byte choice = Byte.parseByte(choiceString);
                    switch(choice){
                        case 1:
                            boolean repeat1 = false;
                            do{
                                try{
                                    System.out.print("Masukkan Jumlah Barang : ");
                                    String sumString = brInput.readLine();
                                    System.out.println();
                                    byte sum = Byte.parseByte(sumString);
                                    if(sum<0){
                                        System.out.println("Jumlah Barang Tidak Boleh Minus");
                                        repeat1 = true;
                                        continue;
                                    }
                                    repeat1 = false;
                                    inputItem(sum);
                                    repeat=repeat();
                                }
                                catch(NumberFormatException e){
                                    System.out.println("Masukkan Angka !");
                                    repeat1 = true;
                                }
                            }while(repeat1);
                            break;
                        case 2:
                            boolean repeat2 = false;
                            do{
                                subMenuShorting();
                                System.out.print("Pilih Menu ==>");
                                String choice2String = brInput.readLine();
                                byte choice2 = Byte.parseByte(choice2String);
                                switch(choice2){
                                    case 1 :
                                        readAndSort(0);
                                        repeat=repeat();
                                        break;
                                    case 2 :
                                        readAndSort(1);
                                        repeat=repeat();
                                        break;
                                    case 3 :
                                        readAndSort(2);
                                        repeat=repeat();
                                        break;
                                    case 4 :
                                        readAndSort(3);
                                        repeat=repeat();
                                        break;
                                    case 5 :
                                        readAndSort(4);
                                        repeat=repeat();
                                        break;
                                    default :
                                        System.out.println("Pilih Hanya Angka 1 sampai 5 !");
                                        repeat = true;
                                        continue;
                                }
                            }while(repeat2);
                            break;
                        case 3 :
                            repeat = false;
                            break;
                        default :
                            System.out.println("Pilih Hanya Angka 1 atau 2 !");
                            repeat = true;
                            continue;
                    }
                }
                catch(NumberFormatException e){
                    System.out.println("Masukkan Angka !");
                    repeat = true;
                }
            }
            catch(IOException e){
            System.out.println("Inputan Salah !");
            repeat = true;
            }
        }
        while(repeat);
    }

    public static void mainMenu(){
        System.out.println("Main Menu");
        System.out.println("1. Input Barang");
        System.out.println("2. Urutkan Barang");
        System.out.println("3. Keluar");
        
    }

    public static void subMenuShorting(){
        System.out.println("Urutkan Barang Berdasarkan");
        System.out.println("1. Kode Barang");
        System.out.println("2. Nama Barang");
        System.out.println("3. Warna Barang");
        System.out.println("4. Jumlah Barang");
        System.out.println("5. Harga Barang");
    }
    
    public static boolean repeat(){
        boolean repeatMethod =  false, repeat = false;
        do{
            try{
                BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Kembali ke Main Menu? Y/N = ");
                String inputRepeat = brInput.readLine();
                if(inputRepeat.equals("Y") | inputRepeat.equals("y")){
                    repeat = true;
                }else if(inputRepeat.equals("N") | inputRepeat.equals("n")){
                    repeat = false;
                }else{
                    System.out.println("Pilih Y atau N");
                    repeatMethod = true;
                    continue;
                }
                repeatMethod = false;
            }catch(IOException e){
                System.out.println("Inputan Salah !");
                repeatMethod = true;
            }
        }while(repeatMethod);
        return repeat;
    }

    public static void inputItem(int sum){
        boolean repeat = false;
        BufferedReader brInput = new BufferedReader(new InputStreamReader(System.in));
        do{
            try{
                String sourceFile = "sourceFile.txt";
                File fileSource = new File(sourceFile);
                FileWriter fileWriterSource = new FileWriter(fileSource.getAbsoluteFile());
                BufferedWriter bwSourceFile = new BufferedWriter(fileWriterSource);

                String[] character = new String[5];
                byte indeksItem=0; 
                while(indeksItem<sum){
                    if(!fileSource.exists()){
                        fileSource.createNewFile();
                    }
                    try{
                        System.out.println("Masukkan Barang ke-"+(indeksItem+1)+" ==> ");
                        System.out.print("Kode Barang (hanya tiga digit, contoh 003) : ");
                        character[0] = brInput.readLine();
                        int code = Integer.parseInt(character[0]);
                        if(code>999){
                            System.out.println("Masukkan hanya tiga digit angka !");
                            continue;
                        }else if(code<0){
                            System.out.println("Kode Tidak Boleh Minus !");
                            continue;
                        }else if(code>=0 && code<10){
                            character[0] = "00"+code;
                        }else if(code>=10 && code<100){
                            character[0] = "0"+code;
                        }                        
                        System.out.print("Nama Barang : ");
                        character[1] = brInput.readLine();
                        System.out.print("Warna Barang : ");
                        character[2] = brInput.readLine();
                        System.out.print("Jumlah Barang : ");
                        character[3] = brInput.readLine();
                        int quantity = Integer.parseInt(character[3]);
                        if(quantity<=0){
                            System.out.println("Jumlah Barang Tidak Boleh Nol atau Minus !");
                            continue;
                        }
                        System.out.print("Harga Barang : Rp. ");
                        character[4] = brInput.readLine();
                        int price = Integer.parseInt(character[4]);
                        if(price<=0){
                            System.out.println("Harga Barang Tidak Boleh Nol atau Minus !");
                            continue;
                        }
                        System.out.println();

                        byte indeksCharacter = 0;
                        while(indeksCharacter<5){
                            if(indeksCharacter==0){
                                bwSourceFile.write("=====");
                                bwSourceFile.newLine();
                            }
                            bwSourceFile.write(character[indeksCharacter]);
                            bwSourceFile.newLine();

                            indeksCharacter++;
                        }

                        indeksItem++;
                    }catch(NumberFormatException e){
                        System.out.println("Masukkan Angka !");
                    }
                }
                bwSourceFile.close();
                repeat = false;
            }
            catch(NegativeArraySizeException e){
                System.out.println("Inputan Tidak Boleh Minus !");
                repeat = true;
            }
            catch(IOException e){
                System.out.println("Inputan Salah !");
                repeat = true;
            }
        }while(repeat);
    }

    public static void readAndSort(int choice){
        boolean repeat = false;
        do{
            String sourceFile = "sourceFile.txt";
            try{
                BufferedReader brCekSourceFile = new BufferedReader(new FileReader(new File(sourceFile)));
                String contentCheck = null;
                byte sumIndeksItem = 0;
                while((contentCheck = brCekSourceFile.readLine()) != null){
                    if(contentCheck.equals("=====")){
                        sumIndeksItem++;
                    }
                }
                
                BufferedReader brSourceFile = new BufferedReader(new FileReader(new File(sourceFile)));
                String content = null;
                
                String[][] character = new String[sumIndeksItem][5];
                byte indeksItem = -1;
                byte indeksCharacter = 0;
                while((content = brSourceFile.readLine()) != null){
                    if(content.equals("=====")){
                        indeksCharacter = 0;
                        indeksItem++;
                        continue;
                    }
                    character[indeksItem][indeksCharacter] = content;
                    indeksCharacter++;
                }
                brSourceFile.close();
                
                String[] newCharacter = new String[sumIndeksItem];
                String exchange;
                for(int i=0; i<sumIndeksItem; i++){
                    newCharacter[i] = character[i][(choice)];
                }
                for(int i=0; i<sumIndeksItem; i++){
                    for(int j=i+1; j<sumIndeksItem; j++){
                        if(newCharacter[i].compareTo(newCharacter[j])>0){
                            for(int k=0; k<sumIndeksItem; k++){
                                exchange = character[i][k];
                                character[i][k] = character[j][k];
                                character[j][k] = exchange;   
                            }
                        }
                    }
                }
                
                String resultFile = "resultFile.txt";
                File fileResult = new File(resultFile);
                FileWriter fileWriterResult = new FileWriter(fileResult.getAbsoluteFile());
                BufferedWriter bwResultFile = new BufferedWriter(fileWriterResult);
                for(int i=0; i<sumIndeksItem; i++){
                    if(!fileResult.exists()){
                        fileResult.createNewFile();
                    }
                    bwResultFile.write("=====");
                    bwResultFile.newLine();
                    for(int j=0; j<5; j++){
                        bwResultFile.write(character[i][j]);
                        bwResultFile.newLine();
                    }
                }
                bwResultFile.close();
                
                System.out.println("Hasil Pengurutan dapat dilihat pada "+resultFile);
            }
            catch(ArrayIndexOutOfBoundsException e){
                System.out.println("Error "+e);
                repeat = true;
            }
            catch(FileNotFoundException e){
                System.out.println("File "+sourceFile+" Tidak ditemukan");
                repeat = false;
            }
            catch(IOException e){
                System.out.println("Inputan Salah !");
                repeat = true;
            }
        }
        while(repeat);
    }
}
